# Count integers in a string.

import tkinter as tk
import re

def string_to_int_list(s:str)->list:
	""" Turns string like "1,2,3" into array [1,2,3] """
	# Remove superfluous characters.
	s=re.sub("[^0-9,]","",s)
	return [int(x) for x in s.split(",") if str.isdigit(x) ]

class Application(tk.Frame):
	def __init__(self, master=None):
		super().__init__(master)
		self.master = master
		self.pack()
		self.create_widgets()

	def create_widgets(self):
		self.entry_label=tk.Label(self,text="Combination")
		self.entry_label.pack()
		self.entry=tk.Entry(self)
		self.entry.pack()
		self.text_label=tk.Label(self,text="Text")
		self.text_label.pack()
		self.text=tk.Text(self)
		self.text.pack(fill=tk.X)
		self.button_count=tk.Button(self,text="Count",command=self.count)
		self.button_count.pack(side=tk.LEFT,expand=tk.TRUE)
		self.button_check=tk.Button(self,text="Check",command=self.check)
		self.button_check.pack(side=tk.LEFT,expand=tk.TRUE)

	def count(self):
		text=self.text.get(1.0,tk.END)
		regex=re.compile(r'[0-9]+')
		numbers=tuple(int(x) for x in regex.findall(text))
		stats=dict((x,numbers.count(x)) for x in set(numbers))
		ordered=sorted(stats.items(),key=lambda x: x[1],reverse=True)
		#self.entry.insert(tk.END,str(ordered))
		out='\n'
		for x in ordered:
			out+="%d occurs %d times.\n"%x
		print(out)
		self.text.insert(tk.END,out)
	
	def check(self):
		combi=string_to_int_list(self.entry.get())
		#print(combi)
		text=self.text.get(1.0,tk.END)
		result=""
		for line in text.split("\n"):
			print("line:",line)
			line_list=string_to_int_list(line)
			#print(line_list)
			shared=set(combi).intersection(line_list)
			print("Shared:",shared)
			result=result+str(line)+"="+str(len(shared))+"\n"
			
		self.text.insert(tk.END,"---\n"+result)

root = tk.Tk()
app = Application(master=root)
#app.entry.insert(tk.END,"Paste here.")
app.mainloop()
