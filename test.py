import re

def string_to_int_list(s:str)->list:
	""" Turns string like "1,2,3" into array [1,2,3] """
	# Remove superfulous characters
	s=re.sub("[^0-9,]","",s)
	return [int(x) for x in s.split(",") if str.isdigit(x) ]

s=" 1,12,43, 4   , 5-èè("

#filtered=list(filter(lambda x:str.isdigit(x) or x is ',',s))
#filtered=re.sub("[^0-9,]","",s)
#print(filtered)
print(string_to_int_list(s))